{
  nixConfig.bash-prompt = "[nix-develop-plutus-tx-spooky:] ";
  description = "A very basic flake";
  inputs.haskellNix.url = "github:input-output-hk/haskell.nix";
  inputs.nixpkgs.follows = "haskellNix/nixpkgs-unstable";
  inputs.flake-utils.url = "github:numtide/flake-utils";
  inputs.plutus.url = "github:input-output-hk/plutus";
  inputs.cardano-node.url = "github:input-output-hk/cardano-node";
  outputs = { self, nixpkgs, plutus, flake-utils, haskellNix, cardano-node }:
    flake-utils.lib.eachSystem [ "x86_64-linux" ] (system:
      let
        deferPluginErrors = true;
        overlays = isTyped: [
          haskellNix.overlay
          (final: prev: {
            # This overlay adds our project to pkgs
            haskellOverrides = hself: hsuper: {
              mkDerivation = args: hsuper.mkDerivation (args //
                (if isTyped then { configureFlags = "-f typed"; } else {}));
            };
            plutus-tx-spooky = final.haskell-nix.project'
              { src = ./.;
                compiler-nix-name = "ghc8107";
                projectFileName = "stack.yaml";
                modules = [{
                  packages = {
                    marlowe.flags.defer-plugin-errors = deferPluginErrors;
                    plutus-use-cases.flags.defer-plugin-errors = deferPluginErrors;
                    plutus-ledger.flags.defer-plugin-errors = deferPluginErrors;
                    plutus-contract.flags.defer-plugin-errors = deferPluginErrors;
                    cardano-crypto-praos.components.library.pkgconfig =
                      pkgs.lib.mkForce [ [ (import plutus { inherit system; }).pkgs.libsodium-vrf ] ];
                    cardano-crypto-class.components.library.pkgconfig =
                      pkgs.lib.mkForce [ [ (import plutus { inherit system; }).pkgs.libsodium-vrf ] ];
                  };
                }];
                shell.tools = {
                  cabal = { };
                  ghcid = { };
                  hlint = { };
                  haskell-language-server = { };
                  stylish-haskell = { };
                };
                # Non-Haskell shell tools go here
                shell.buildInputs = with pkgs; [
                  nixpkgs-fmt
                  cardano-node.outputs.packages.x86_64-linux."cardano-node:exe:cardano-node"
                  cardano-node.outputs.packages.x86_64-linux."cardano-cli:exe:cardano-cli"
                ];
              };
          })
        ];
        pkgs = import nixpkgs { inherit system; overlays = (overlays false); inherit (haskellNix) config; };
        pkgs-typed = import nixpkgs { inherit system; overlays = (overlays true); inherit (haskellNix) config; };
        flake-untyped = pkgs.plutus-tx-spooky.flake { };
        flake-typed   = pkgs-typed.plutus-tx-spooky.flake { };
      in
      flake-untyped // {
        packages =
            (pkgs.lib.mapAttrs' (name: value: { name = "${name}-typed"; inherit value; }) flake-typed.packages)
         // (pkgs.lib.mapAttrs' (name: value: { name = "${name}-untyped"; inherit value; }) flake-untyped.packages);
        defaultPackage = flake-untyped.packages."plutus-tx-spooky:lib:plutus-tx-spooky";
        devShell = flake-untyped.devShell.overrideAttrs (oa: {
          shellHook = oa.shellHook + ''
            alias haskell-language-server-wrapper=haskell-language-server
            ${pkgs.lolcat}/bin/lolcat ${./intro}
          '';
        });
      });
}
